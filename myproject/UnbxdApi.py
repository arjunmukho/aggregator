from UnbxdException import UnbxdException
from UnbxdParams import UnbxdParams
from UnbxdService import UnbxdService
class UnbxdApi:
	def __init__(self, siteName, apiKey, transport="http"):
		try:
			self._params = UnbxdParams()
			self.setSiteName(siteName)
			self.setApiKey(apiKey)
			self.setTransport(transport)

		except Exception as e:
			raise UnbxdException(str(e))
	
	def getSiteName(self):
		return self._siteName
	
	def setSiteName(self,siteName):
		if(siteName == None):
			raise UnbxdException(UnbxdException.getNOSITENAME())
		self._siteName = siteName
	
	def getApiKey(self):
		return self._apiKey
	
	def setApiKey(self,apiKey):
		if apiKey == None :
			raise UnbxdException(UnbxdException.getNOAPIKEY())
		self._apiKey = apiKey
	
	def prepareSearch(self,query):
		self._params.setRuleSet("search")
		self._params.setQuery(query)
		return self
	
	def prepareBrowse(self,categoryId):
		self._params.setRuleSet("browse")
		self._params.setCategoryId(categoryId)
		return self
	
	def setRuleSet(self,ruleset):
		self._params.setRuleSet(ruleset)
		return self
	
	def addSort(self,field,value) :
		try:
			self._params.setSort(field, value)
			return self

		except Exception as e:
			raise UnbxdException(str(e))
		
	def addAttributeFilter(self,field,value = None) :
		try:
			if value == None:
				for i in field:
					self._params.setFilter(i, field[i])
			else:	
				#map of single string to multiple strings
				self._params.setFilter(field, value)
			return self
		except Exception as e:
			print e
			raise UnbxdException(str(e))
		
	def addRangeFilter(self,field, value = None) :
		try:
			if value == None:
				for i in field:
					self._params.setFilter(i, field[i])
			else:
				self._params.setFilter(field, value)
			return self
		except Exception as e:
			raise UnbxdException(str(e))
		
	def setStart( self, start):
		self._params.setStart(start)
		return self
	

	def setLimit( self, limit):
		self._params.setLimit(limit)
		return self
	
	def setDebug( self, debug):
		self._params.setDebug(debug)
		return self
	
	def  getTransport(self):
		return self._transport
	
	def setTransport(self, transport):
		self._transport = transport
	
	def getResults(self) :
		if self.getTransport() != None :
			address=self.getTransport()
		else: 
			address="http"
		address+= "://"+ self.getSiteName()+ ".search.unbxdapi.com/" + self.getApiKey()
		print address
		unbxdServiceObject = UnbxdService()
		response = unbxdServiceObject.search(self._params, address, False)
		return response