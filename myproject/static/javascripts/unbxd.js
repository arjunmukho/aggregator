var unbxdApiObj = {

	apiKey: "",
	siteUrl: "",
	noPerPage: 20,
	iSiteName: "demo-u1393483043451",
	ruleset: "",
	groupLimit: 3,
	pageNo: 1,
	isBrowse: false,
	navId: -1,
	navNode : new Array(),
	fqs: new Array(),//Datastore for active filters
	filtered: new Array(),
	nodes: new Array(),
	tree: new Array,
	facetFl: new Array(),//List of fields for faceting	
	personnaId: 0,
	setPersonnaId: function(val){
		this.personnaId = val;
	},
	addFq: function(fieldName, fieldVal){

		for(var count=0; count<this.fqs.length;count++){
			if(this.fqs[count].fieldName == fieldName){
				this.fqs[count].fieldVal[this.fqs[count].fieldVal.length]=fieldVal;
				return;
			}
		}

		var fqObj = this.fqs[this.fqs.length] = {};
		fqObj.fieldName = fieldName;
		fqObj.fieldVal = new Array();
		fqObj.fieldVal[fqObj.fieldVal.length]=fieldVal;

		
	},
	removeFq: function(fieldName,fieldVal){

		for( var count=0; count<this.fqs.length;count++){
			if(this.fqs[count].fieldName == fieldName){
				if(this.fqs[count].fieldVal.length<2){
					this.fqs.remove(count);
				} else {
					for ( var valCount=0; valCount<this.fqs[count].fieldVal.length;valCount++){
						if(this.fqs[count].fieldVal[valCount]==fieldVal)
							this.fqs[count].fieldVal.remove(valCount);
					}
				}
					

				break;
			}
		}

	},
	addFacetFl: function(fieldName){
		this.facetFl[this.facetFl.length]=fieldName;

	},
	clearFq: function(){

		this.fqs = new Array();
	},
	setSiteUrl: function (){

	},
	setApiKey: function () {

	},
	setGroupLimit: function(limit){
		this.groupLimit = limit;
	},
	isFacetSelected: function(fieldName){

		for(var count=0; count<this.fqs.length;count++){
			if(this.fqs[count].fieldName == fieldName){
				var fqObj = {};
				fqObj.fieldName = this.fqs[count].fieldName;
				fqObj.fieldVal = this.fqs[count].fieldVal;

				return fqObj;

			}
		}

		return null;

	},
	getGroupedResults: function (q, groupField, ruleset, callback, pageNo, noPerPage){

		var groupUrlStr = "&group.limit="+this.groupLimit+"&group=true&group.field="+groupField;

		var reqUrl = this.getReqUrl(q,ruleset,pageNo, noPerPage)+groupUrlStr;

		$.ajax({
	        url: reqUrl,
	        type: "post",
	        dataType: "json",
	        success: callback
	    });
	},

	getSearchResults: function(q, ruleset,callback, pageNo, noPerPage){
	    console.trace();
		this.pageNo = pageNo;
		this.noPerPage = noPerPage;

		if(ruleset == "browse" || this.navId != -1){
			this.navId = q;
			var reqUrl = this.getReqUrl(q, ruleset, pageNo, noPerPage);
			/*var reqUrl = 	this.siteUrl+"/"+this.apiKey+"/"+ruleset+"?category-id="+q+
			"&start="+((pageNo-1)*noPerPage)+"&rows="+noPerPage+"&wt=json";	*/
			this.isBrowse = true;
		}else{

			var reqUrl = this.getReqUrl(q,ruleset,pageNo, noPerPage);
			this.isBrowse = false;
		}

		$.ajax({
	        url: reqUrl,
	        type: "post",
	        dataType: "jsonp",
	        jsonp: 'json.wrf',	        
	        success: callback        
	    });

	    
	},

	getRecommendations: function(uid){

		//var recUrl =  "http://apac-recommendations.unbxdapi.com/v1.0/" + this.apiKey + "/" + this.iSiteName + "/recommend/?uid=" + uid + "&alignment=hz&cont=recommended-for-you";
		var ubx = document.createElement('script');
		ubx.type = 'text/javascript';
		ubx.async = true;
        /*ubx.src = "http://apac-recommendations.unbxdapi.com/v1.0/" + this.apiKey + "/" + this.iSiteName + "/recommend/?uid=" + uid + "&alignment=hz&cont=recommended-for-you"*/
        ubx.src = "http://apac-recommendations.unbxdapi.com/v1.0/" + this.apiKey + "/" + this.iSiteName + "/recommend/"+ uid + "?alignment=hz&cont=recommended-for-you";

        console.log(ubx.src);
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ubx);
	},

	getRecentlyViewed: function(uid){
		var ubx = document.createElement('script');
		ubx.type = 'text/javascript';
		ubx.async = true;
        ubx.src = "http://apac-recommendations.unbxdapi.com/v1.0/master_key/" + this.iSiteName + "/recently-viewed/" + uid + "?alignment=hz&cont=recently-viewed-widget";
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ubx);
	},


	getMoreLikeThese: function(pid , callback){

		var recUrl =  "http://apac-recommendations.unbxdapi.com" + "/" + "v1.0" + "/" + "master_key" + "/" + this.iSiteName + "/" 
						+ "more-like-these/"  + pid  + "?format=json&limit=7";

		console.log(recUrl);
		
		$.ajax({
	        url: recUrl,
	        type: "POST",
	        //headers: { 'Access-Control-Allow-Origin': '*' },
	        dataType: "jsonp",
	        jsonp: 'json.wrf',
	        success: callback	        
	        /*success: function(data){
	        	console.log(data);
	        }
	        error: function(data){
	        	console.log(data);
	        }*/
	    });

	},
	
	getautoSearchResults: function(q,fq, ruleset,callback, pageNo, noPerPage){	

		this.pageNo = pageNo;
		this.noPerPage = noPerPage;

		if(ruleset == "browse" || this.navId != -1){
			this.navId = q;
			var reqUrl = this.getReqUrl(q, ruleset, pageNo, noPerPage);
			/*var reqUrl = 	this.siteUrl+"/"+this.apiKey+"/"+ruleset+"?category-id="+q+
			"&start="+((pageNo-1)*noPerPage)+"&rows="+noPerPage+"&wt=json";	*/
			this.isBrowse = true;
		}else{ 
			var reqUrl = this.getReqUrl(q,ruleset,pageNo, noPerPage);
			reqUrl=reqUrl+"&filter=Category_fq:\""+fq+"\"";
			this.isBrowse = false;
		}

		$.ajax({
	        url: reqUrl,
	        type: "post",
	        dataType: "jsonp",
	        jsonp: 'json.wrf',	        
	        success: callback        
	    });

	    
	},
	getSearchResultsScroll: function(q, ruleset,callback){	

		var reqUrl = this.getReqUrl(q,ruleset,this.pageNo+1, this.noPerPage);
		this.pageNo++;

		$.ajax({
	        url: reqUrl,
	        type: "post",
	        dataType: "jsonp",
	        jsonp: 'json.wrf',	        
	        success: callback        
	    });

	    
	},
	getReqUrl: function (q, ruleset, pageNo, noPerPage){
		var me = this;
		if(q.length == 0){
			q="*";
		}

		if(ruleset == "browse"){
			this.navId = q;
			var reqUrl = 	this.siteUrl+"/"+this.apiKey+"/"+ruleset+"?category-id="+q+
			"&start="+((pageNo-1)*noPerPage)+"&rows="+noPerPage+"&wt=json";
				
			this.isBrowse = true;
		}else{ 
			var reqUrl = this.siteUrl+"/"+this.apiKey+"/"+ruleset+"?q="+q+
				"&start="+((pageNo-1)*noPerPage)+"&rows="+noPerPage+"&wt=json";

				
			this.isBrowse = false;
		}
		

		if(q == "*"){
			reqUrl += "&filter=-autosuggest:*"
		}

		for(var count=0; count<this.fqs.length;count++){
			if(this.fqs[count].fieldName == "Price_fq"){
				reqUrl += '&fq={!tag='+this.fqs[count].fieldName+'}'+'('

				for ( var valCount=0; valCount<this.fqs[count].fieldVal.length;valCount++){

					if(valCount>0)reqUrl+= ' OR '

					reqUrl+=this.fqs[count].fieldName+':'+this.fqs[count].fieldVal[valCount];
				}
				reqUrl+=')';

					
			} else {
				reqUrl += '&fq={!tag='+this.fqs[count].fieldName+'}'+'('

				for ( var valCount=0; valCount<this.fqs[count].fieldVal.length;valCount++){

					if(valCount>0)reqUrl+= ' OR '

					reqUrl+=this.fqs[count].fieldName+':"'+escape(this.fqs[count].fieldVal[valCount])+'"';
				}
				reqUrl+=')'
					
			}
		}	

		for ( var facetCount=0; facetCount<this.facetFl.length;facetCount++){
			reqUrl += '&facet.field='+( filterSelected(this.facetFl[facetCount])? "{!ex="+this.facetFl[facetCount]+"}":""  )
					+this.facetFl[facetCount];
		}

		reqUrl += addBoost(this.personnaId);

		function filterSelected(fieldName){
			for (var fCount=0;fCount<me.fqs.length;fCount++){
				if(me.fqs[fCount].fieldName == fieldName) return true;
			}
			return false;
		}

		function addBoost(val){
			if(val==1){

				return "&boost=if(or(comparerange(price,0,13,'less than'),comparerange(price,88,100,'in between'),comparerange(price,269,271,'less than')),2,1)";
			} else if (val ==2) {

				return "&boost=if(compare('gender_fq','men','equal%20to'),4,1)&boost=if(compare('category_fq','Sneakers','equal%20to'),2.5,1)"
					+"&boost=if(or(compare('category_fq','Dress%20Shoes','equal%20to'),compare('category_fq','Boots','equal%20to')),.4,1)"
					+"&boost=if(compare('category_fq','Casual Shirts','equal%20to'),2,1)";

			}else if (val ==3 ) {

				return "&boost=if(compare('gender_fq','men','equal%20to'),4,1)&boost=if(compare('category_fq','Dress%20Shoes','equal%20to'),2,1)"
				+"&boost=if(or(comparerange(price,400,500,'equal%20to'),comparerange(price,100,150,'equal%20to')),2,1)"
				+"&boost=if(compare('category_fq','Dress Shirts','equal%20to'),2,1)";
			} else if (val ==4 ){

				return "&boost=if(compare('gender_fq','women','equal%20to'),4,1)"
				+"&boost=if(compare('category_fq','Pumps','equal%20to'),2,1)";
			}
			return "";
		}

		return reqUrl;
	},
	loadNavigationNode: function (callback){

		var navUrl = this.siteUrl+"/"+this.apiKey+"/browse?category-id=0&type=navigate&wt=json&indent=on&boost=log(price)";
		this.navNode;

		$.ajax({
	        url: navUrl,
	        type: "POST",
	        dataType: "json",
	        success: callback
	    });

		
	},

	getNavTree: function(tree){

		var data = tree["Taxonomy Nodes "];

		//this.tree = tree["Taxonomy Nodes "];

		/*for( var count=0; count<nodes.length; count ++ ){

		//if(nodes.parent)
	    	var isPresent = false;

	    	for( var count1 =0; count1< this.navNode.length; count1++) {
	    		if(this.navNode[count1].parentNode == nodes[count].parent){

	    			isPresent = true;

	    			var childNode = this.navNode[count1].childs[this.navNode[count1].childs.length] = {};
	    			childNode.id = nodes[count].id;
	    			childNode.name = nodes[count].name;
	    			childNode.parent = nodes[count].parent;

	    			break;
	    		}
	    	}

	    	if(!isPresent) {

	    		var navNodeEl =  this.navNode[this.navNode.length] = {};

	    		navNodeEl.parentNode = nodes[count].parent;

	    		navNodeEl.childs = {};
	    		navNodeEl.childs.id = nodes[count].id;
	    		navNodeEl.childs.name = nodes[count].name;
	    		navNodeEl.childs.parent = nodes[count].parent;

	    	}
	    }

	    return this.navNode;
*/
		console.log(data);
		var treeObj = [];

		function getChildren(parent, obj){
		    var arr = [];
		    var parentObj = {};
		    for(var j=0; j < obj.length; j++){
		        if(parent === obj[j].parent){
		            arr.push(obj[j]);
		        }
		    }
		    return arr;
		}

		var parents = [],
			nodes = [],
			tempTree = [];

		for(var i=0; i < data.length; i++){
			if(data[i].parent !== "-1") {
				parents.push(parseInt(data[i].parent, 10));
				nodes.push(data[i].id);
				tempTree.push(data[i]);
			}
		}

		parents = parents.sort(function(a,b){return a - b});
		var filtered = parents.filter(function(val, index, arr){
		    return index == arr.indexOf(val);
		});

		for(var bles = 0; bles < filtered.length; bles++){
			filtered[bles] = filtered[bles].toString();
		}

		this.filtered = filtered;
		this.nodes = nodes;
		this.tree = tempTree;

		console.log(filtered);

		for(var k=0; k < filtered.length; k++){
		    var obj = {};
		    for(var l=0; l < data.length; l++){
		        if(filtered[k] === "1"){
		            obj.id = 1;
		            obj.name = "Homepage";
		            obj.url = "";
		            obj.parent = "-1";
		            obj.children = getChildren(filtered[k], data);
		        } else if(filtered[k] === data[l].id.toString()){
		            obj.id = data[l].id;
		            obj.parent = data[l].parent;
		            obj.name = data[l].name;
		            obj.url = data[l].url;
		            obj.children = getChildren(filtered[k], data);
		        }
		    }
		    treeObj.push(obj);
		}

		return treeObj;
	}
};

var UnbxdApi = function (apiKey, siteUrl){
	this.apiKey = apiKey;
	this.siteUrl = siteUrl;
	this.fqs = new Array();
}

UnbxdApi.prototype = unbxdApiObj;


Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

function checkKey(el, dummy, e) {
	var keynum;
	if (window.event) { // IE
	keynum = e.keyCode
	} else if (e.which) { // Netscape/Firefox/Opera
	keynum = e.which
	}
	if (keynum == 13) { 
		$("#displayResult").html('<div class="row"><div class="span9">'
				+'<h2 class="title">Search Results for : '+ $("#exampleSearch").val()
				+'</h2><hr></div></div>');
		$("#spellHolder").empty();
		/*$("#productsListHolder").empty();
		$("#ajax_listOfOptions").empty();*/
		var searchin = $("#exampleSearch").val().split(" in ");
		if(searchin[1] != null){
	//	unbxdApi.getautoSearchResults(searchin[0].trim(),searchin[1].trim(),'search',paintHomePage,1,12);
		}else{
		    searchText = searchin[1];
			unbxdApi.getSearchResults($("#exampleSearch").val(),'search',paintHomePage,1,15);
			//unbxdApi.getAlsoViewed("uid-1389610082013-80416",paintAlsoViewPage);
			
		}	//querySearch("",0,true,"");
	}else {
		//ajax_showOptions(el,'',e);
		ajax_showOptions(el,'',e,"demo-u1393483043451","ae30782589df23780a9d98502388555f");
	}
	return false;
	}
	function trimText(txt){
	if(txt.length>20){
	txt = txt.substring(0,19)+'..';
	}
	return txt;
	}

function getChildren(parent, obj){
    var arr = [];
    var parentObj = {};
    for(var j=0; j < obj.length; j++){
        if(parent === obj[j].parent){
            arr.push(obj[j]);
        }
    }
    return arr;
} 
