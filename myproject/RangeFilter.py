class RangeFilter:

	def getFrom(self):
		return self._fromRange

	def setFrom(self, fromRange):
		self._fromRange = fromRange

	def getTo(self):
		return self._toRange

	def setTo(self, toRange):
		self._toRange = toRange
