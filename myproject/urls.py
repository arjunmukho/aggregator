from django.conf.urls import *
import views
#urlpatterns = patterns('', (r'^$', views.home), 
#	)


urlpatterns = patterns('',
    url(r'^$', views.home),
    #url(r'^search*$', views.search),
    #url(r'^search(?P<searchQuery>\w{0,50})/$',views.search),
    url(r'^search/$',views.search),
    url(r'^browse/$',views.browse),
)
